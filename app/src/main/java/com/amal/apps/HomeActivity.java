package com.amal.apps;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

public class HomeActivity extends AppCompatActivity {

    ImageView  menu_peta, menu_faskes, menu_info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        menu_peta = (ImageView) findViewById(R.id.menu_peta);//tinggal copy kalo mau nambah
        menu_faskes = (ImageView) findViewById(R.id.menu_faskes);
        menu_info = (ImageView) findViewById(R.id.menu_info);

        //ini juga tinggal copy kalo mau nambah
        menu_peta.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, PetaActivity.class);
                startActivity(intent);
            }
        });

        menu_faskes.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, FasKesActivity.class);
                startActivity(intent);
            }
        });

        menu_info.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, InfoActivity.class);
                startActivity(intent);
            }
        });

    }
}
